module Gene
  module Types
  end
end

require 'gene/types/stream'
require 'gene/types/noop'
require 'gene/types/ident'
require 'gene/types/ref'
require 'gene/types/placeholder'
require 'gene/types/group'
require 'gene/types/complex_string'
require 'gene/types/base64'
